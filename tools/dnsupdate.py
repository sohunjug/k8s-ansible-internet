#!/usr/bin/python
# __*__ coding: utf-8 __*__
# Filename: dnsupdate.py

import os
import sys
import socket
import select
import dns.query
import dns.update
import dns.resolver

DOMAIN = 'kayak.local'
SERVER = '127.0.0.1'

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
resolver = dns.resolver.Resolver()
resolver.nameservers = [socket.gethostbyname('127.0.0.1')]
try:
    s.bind(('0.0.0.0', 9000))
    s.listen(1)
except Exception as e:
    raise e

for i in range(1, 4):
    pid = os.fork()
    if pid < 0:
        print('fork error')
        sys.exit(-1)
    elif pid > 0:
        print('fork process %d' % pid)
    else:
        pass

while 1:
    flag = True
    deleteFlag = False
    client, address = s.accept()
    try:
        data = client.recv(256)
        domain = str(data).split(' = ')
        print("%s get a client[%s] from %s\n%s %s" %
              (os.getpid(), str(client), address, domain[0], domain[1]))
        answer = resolver.query(domain[0] + '.' + DOMAIN, 'A')
        for rdata in answer:
            deleteFlag = True
            if rdata == domain[1]:
                flag = False
        if flag:
            up = dns.update.Update(DOMAIN)
            if deleteFlag:
                up.delete(domain[0])
            up.add(domain[0], 300, 'A', domain[1])
            dns.query.tcp(up, SERVER)
    except Exception as e:
        print(e)
    client.close()
